<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Product;

class ProductControleur extends Controller
{
    /*
    *fonction de creation de l'article
    */
function creeProduct(Request $request){

    $product = new Product;
    $product->titre = $request->titre;
    $product->description =$request->description;
    $product->vendeur =$request->vendeur;
    $product->image =$request->image;
    $product->save();
    return response()->json([
        "message"=>"creation de produit reussi",
        "products"=>$product,
        ],201);
   }

}